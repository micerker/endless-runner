﻿using System;
using System.Collections;
using Spriter2UnityDX;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    [SerializeField] private int coinValue = 1;
    private Animator anim;
    [SerializeField] private GameObject effect;
    private SpriteRenderer renderer;


    void Start()
    {
        effect.SetActive(false);
        anim = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Player")
        {
            FindObjectOfType<AudioManager>().Play("CoinCollected");
            effect.SetActive(true);
            renderer.sortingOrder = -5;
            CoinCalc.instance.ChangeScore(coinValue);
        }
            
    }

    IEnumerator Delay(int time)
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(1f);
            time--;
        }

        yield return new WaitForSeconds(1f);
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
