﻿using UnityEngine;
using UnityEngine.UI;
public class CoinCalc : MonoBehaviour
{
    public static CoinCalc instance;
    public Text coinScore;
    public static int coin;
    public static int bestCombo;
    public static int totalCoins;
    // Start is called before the first frame update
    void Start()
    {
        coin = 0;
        totalCoins = PlayerPrefs.GetInt("totalcoins");
        bestCombo = PlayerPrefs.GetInt("bestcombo");
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ChangeScore(int coinValue)
    {
        coin += coinValue;
        coinScore.text = coin.ToString();
    }

    private void OnDestroy()
    {
        totalCoins += coin;
        if (coin > bestCombo)
        {
            bestCombo = coin;
        }
        PlayerPrefs.SetInt("totalcoins",totalCoins);
        PlayerPrefs.SetInt("bestcombo",bestCombo);
        PlayerPrefs.Save();
    }
}
