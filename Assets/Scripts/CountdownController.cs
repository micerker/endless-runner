using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CountdownController : MonoBehaviour
{
    public int countdownTime;
    public Text countdownDisplay;
    [SerializeField] private GameObject countdownMenu;
    [SerializeField] private Animator anim; 
    void Start()
    {
        anim.SetBool("Idle", true);
        anim.SetBool("Run",false);
        StartCoroutine(CountdownToResume());
    }
    IEnumerator CountdownToResume()
    {
        while (countdownTime > 0)
        {
            countdownDisplay.text = countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }
        countdownDisplay.text = "GO !";
        anim.SetBool("Idle", false);
        anim.SetBool("Run", true);
        yield return new WaitForSeconds(1f);
        countdownMenu.SetActive(false);
    }
}
