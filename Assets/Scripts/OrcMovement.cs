﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private bool moveRight=true;
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (moveRight)
        {
            transform.Translate(0.75f*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(0.5f, 0.5f);
        }
        else
        {
            transform.Translate(-0.75f*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(-0.5f, 0.5f);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            FindObjectOfType<AudioManager>().Play("EnemyDeath");
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Turn"))
        {
            if (moveRight)
            {
                moveRight = false;
            }
            else
            {
                moveRight = true;
            }
        }
    }
    private IEnumerator Delay(int time)
    {
        yield return new WaitForSeconds(time);
    }
}
