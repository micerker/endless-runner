﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCalc : MonoBehaviour
{
    public Transform player;
    public static int distance;
    public static int bestdistance;
    public static int totaldistance;
    public Text score;
    // Update is called once per frame
    private void Start()
    {
        totaldistance = PlayerPrefs.GetInt("totaldistance");
        bestdistance = PlayerPrefs.GetInt("bestdistance");
    }

    void FixedUpdate()
    {
        distance = (int)(player.position.x + 6);
        score.text = distance.ToString("0")+" m";
        if (distance > bestdistance)
        {
            bestdistance = distance;
        }
    }

    private void OnDestroy()
    {
        totaldistance += distance;
        PlayerPrefs.SetInt("totaldistance",totaldistance);
        PlayerPrefs.SetInt("bestdistance",bestdistance);
        PlayerPrefs.Save();
    }
}
