﻿using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject inGame;
    public Text yourDistance, yourCombo;
    // Update is called once per frame
    
    void Update()
    {
        if (gameOver.activeSelf)
        {
            WaitForRestart();
        }
    }

    void WaitForRestart()
    {
        inGame.SetActive(false);
        gameOver.SetActive(true);
        yourDistance.text = ScoreCalc.distance.ToString()+ "m";
        yourCombo.text = CoinCalc.coin.ToString();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
