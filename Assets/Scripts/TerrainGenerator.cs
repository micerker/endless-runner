﻿using System.Collections.Generic;
using UnityEngine;
public class TerrainGenerator : MonoBehaviour
{
    private const float PLAYER_DISTANCE_SPAWN_TERRAIN_PART = 50f;
    [SerializeField] private Transform terrainPartStart;
    [SerializeField] private List<Transform> terrainPartList;
    [SerializeField] private Transform player;
    private Vector3 lastEndPosition;

    void Awake()
    {
        lastEndPosition = terrainPartStart.Find("EndPosition").position;
    }

    void Update()
    {
        if (Vector3.Distance(player.position, lastEndPosition) < PLAYER_DISTANCE_SPAWN_TERRAIN_PART)
        {
            SpawnTerrainPart();
        }
    }

    void SpawnTerrainPart()
    {
        Transform chosenTerrainPart = terrainPartList[Random.Range(0, terrainPartList.Count)];
        Transform lastTerrainPartTransform = SpawnTerrain(chosenTerrainPart, lastEndPosition + new Vector3(1.75f,0,0));
        lastEndPosition = lastTerrainPartTransform.Find("EndPosition").position;
    }

    Transform SpawnTerrain(Transform terrainPart, Vector3 spawnPosition)
    {
        Transform terrainPartTransform = Instantiate(terrainPart, spawnPosition, Quaternion.identity);
        return terrainPartTransform;
    }
}
