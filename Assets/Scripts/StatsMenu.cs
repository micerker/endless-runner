﻿using UnityEngine;
using TMPro;
public class StatsMenu : MonoBehaviour
{
    public TMP_Text bestDistance,totalDistance,bestCombo,totalCoins;
    // Start is called before the first frame update
    void Start()
    {
        totalDistance.text = PlayerPrefs.GetInt("totaldistance", 0).ToString();
        bestDistance.text = PlayerPrefs.GetInt("bestdistance",0).ToString();
        totalCoins.text = PlayerPrefs.GetInt("totalcoins", 0).ToString();
        bestCombo.text = PlayerPrefs.GetInt("bestcombo", 0).ToString();
    }
}
