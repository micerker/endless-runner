using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class InGameController : MonoBehaviour
{
    [SerializeField] private GameObject tutorial;
    void Start()
    {
        if (PlayerPrefs.GetInt("PlayTime")==0)
        {
            Time.timeScale = 0f;
            tutorial.SetActive(true);
            PlayerPrefs.SetInt("PlayTime", 1);
            PlayerPrefs.Save();
        }
        else
        {
            tutorial.SetActive(false);
        }
    }

    void Update()
    {
        if (!tutorial.activeSelf)
        {
            Time.timeScale = 1f;
        }
    }

    private IEnumerator Delay(int time)
    {
        yield return new WaitForSeconds(time);
    }
}
