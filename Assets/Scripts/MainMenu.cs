﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private Text bestDistance,bestCombo,totalCoins;
    [SerializeField] private GameObject player;
    [SerializeField] private GameController controller;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = player.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if(mainMenu.activeSelf){
            WaitForPlay();
        }
    }

    void WaitForPlay()
    {
        mainMenu.SetActive(true);
        totalCoins.text = PlayerPrefs.GetInt("totalcoins").ToString();
        bestDistance.text = PlayerPrefs.GetInt("bestdistance").ToString() +" m";
        bestCombo.text = PlayerPrefs.GetInt("bestcombo").ToString();
    }
    public void PlayGame()
    {
        mainMenu.SetActive(false);
    }
}
