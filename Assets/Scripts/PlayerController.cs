﻿using System;
using System.Collections;
using Spriter2UnityDX;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb,enemyRb;
    public float speed;
    [SerializeField] private float jumpForce;
    private bool groundedCheck,attackCheck;
    private EntityRenderer renderer,enemyRenderer;
    [SerializeField] private float targetSpeed = 40f;
    private Animator anim,enemyAnim;
    private GameController controller;
    private GameObject enemyDeath;
    private bool canDoubleJump;
    [SerializeField] private GameObject mainMenu,creditsMenu,gameOverMenu,pauseMenu,countdownMenu;
    [SerializeField] private GameObject deathEffect;
    [SerializeField] private GameObject attackEffect;
    [SerializeField] private GameObject jumpEffect;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private float attackRange = 0.5f;
    [SerializeField] private LayerMask enemyLayers;
    
    // Start is called before the first frame update
    void Start()
    {
        canDoubleJump = false;
        groundedCheck = false;
        attackCheck = false;
        rb.GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        renderer = GetComponent<EntityRenderer>();
        controller = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        attackPoint.gameObject.SetActive(false);
        jumpEffect.SetActive(false);
        attackEffect.SetActive(false);
        if (!mainMenu.activeSelf && !creditsMenu.activeSelf && !gameOverMenu.activeSelf && !pauseMenu.activeSelf && !countdownMenu.activeSelf)
        {
            anim.SetBool("Run", true);
            if (attackCheck)
            {
                StartCoroutine(Delay(5));
                attackCheck = false;
                anim.SetBool("Attack", false);
            }
            rb.velocity = new Vector2(speed, rb.velocity.y);
            if (speed < targetSpeed)
            {
                speed += 0.002f;
                
            }

            if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began &&
                  Input.GetTouch(0).position.x < Screen.width / 2 &&
                  Input.GetTouch(0).position.y < Screen.height * 0.75f) || (Input.GetMouseButtonDown(0) &&
                                                                            Input.mousePosition.x <
                                                                            Screen.width / 2 &&
                                                                            Input.mousePosition.y <
                                                                            Screen.height * 0.75f) ||
                 (Input.GetKeyDown(KeyCode.Space))) && groundedCheck)
            {
                jumpEffect.SetActive(true);
                FindObjectOfType<AudioManager>().Play("Jump");
                anim.SetBool("Jump", true);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                anim.SetBool("Jump", false);
                anim.SetBool("Grounded", false);
                groundedCheck = false;
            }
            else if (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began &&
                       Input.GetTouch(0).position.x < Screen.width / 2 &&
                       Input.GetTouch(0).position.y < Screen.height * 0.75f) || (Input.GetMouseButtonDown(0) &&
                          Input.mousePosition.x <
                          Screen.width / 2 &&
                          Input.mousePosition.y <
                          Screen.height * 0.75f) ||
                      (Input.GetKeyDown(KeyCode.Space))) && canDoubleJump)
            {
                jumpEffect.SetActive(true);
                FindObjectOfType<AudioManager>().Play("Jump");
                anim.SetBool("Jump", true);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                anim.SetBool("Jump", false);
                anim.SetBool("Grounded", false);
                canDoubleJump = false;
            }
            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began &&
                 Input.GetTouch(0).position.x > Screen.width / 2 && Input.GetTouch(0).position.y < Screen.height * 0.75f) ||
                (Input.GetMouseButtonDown(0) && Input.mousePosition.x > Screen.width/2 &&
                 Input.mousePosition.y < Screen.height * 0.75f) || (Input.GetKey(KeyCode.Keypad0)))
            {
                attackPoint.gameObject.SetActive(true);
                attackEffect.SetActive(true);
                anim.SetBool("Attack", true);
                FindObjectOfType<AudioManager>().Play("Attack");
                attackCheck = true;
                Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
                foreach (Collider2D hitEnemy in hitEnemies)
                {
                    if (!hitEnemy.isTrigger)
                    {
                        FindObjectOfType<AudioManager>().Play("EnemyDeath");
                        hitEnemy.isTrigger = true;
                        enemyRb = hitEnemy.gameObject.GetComponent<Rigidbody2D>();
                        enemyRenderer = hitEnemy.gameObject.GetComponent<EntityRenderer>();
                        enemyRb.constraints = RigidbodyConstraints2D.FreezeAll;
                        enemyRenderer.SortingOrder = -5;
                    }
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
        {
            anim.SetBool("Grounded",true);
            groundedCheck = true;
            canDoubleJump = true;
        } 
        if (other.gameObject.tag.Equals("Edge"))
        {
            FindObjectOfType<AudioManager>().Play("Death");
            anim.SetBool("Hurt", true);
            StartCoroutine(Delay(1));
            deathEffect.SetActive(true);
            Debug.Log("You Lose");
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
            renderer.SortingOrder = -5;
            controller.EndGame();
        }
        if (other.gameObject.tag.Equals("Enemies"))
        {
            if (!attackCheck)
            {
                FindObjectOfType<AudioManager>().Play("Death");
                deathEffect.SetActive(true);
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
                controller.EndGame();
                renderer.SortingOrder = -5;
            }
        }
    }

    private IEnumerator Delay(int time)
    {
        yield return new WaitForSeconds(time);
    }

    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position,attackRange);
    }
}
