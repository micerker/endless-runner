﻿using UnityEngine;

public class EdgeController : MonoBehaviour
{
    [SerializeField] private Transform camera;
    [SerializeField] private Vector3 offset;
    
    void FixedUpdate()
    {
        transform.position = camera.position + offset;
    }
}
