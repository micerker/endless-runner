using System;
using Spriter2UnityDX;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private bool moveRight=true;
    [SerializeField] private GameObject deathEffect;
    private Animator anim;
    private Collider2D collider;
    private Rigidbody2D rb;
    private EntityRenderer renderer;
    private bool attackCheck;
    private void Start()
    {
        attackCheck = false;
        deathEffect.SetActive(false);
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        renderer = GetComponent<EntityRenderer>();
    }

    private void FixedUpdate()
    {
        if (moveRight)
        {
            transform.Translate(0.75f*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(0.4f, 0.4f);
        }
        else
        {
            transform.Translate(-0.75f*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(-0.4f, 0.4f);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            deathEffect.SetActive(true);
            renderer.SortingOrder = -5;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("AttackPoint"))
        {
            deathEffect.SetActive(true);
            renderer.SortingOrder = -5;
        }
        if (other.gameObject.tag.Equals("Turn"))
        {
            if (moveRight)
            {
                moveRight = false;
            }
            else
            {
                moveRight = true;
            }
        }

        if (other.gameObject.tag.Equals("Vision"))
        {
            if (other.gameObject.name.Equals("Right"))
            {
                if (moveRight)
                {
                    moveRight = false;
                }
            }
            else if (other.gameObject.name.Equals("Left"))
            {
                if (moveRight == false)
                {
                    moveRight = true;
                }
            }
            attackCheck = true;
            anim.SetBool("Attack", true);
        }
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
