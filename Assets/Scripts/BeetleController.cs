﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeetleController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private bool moveRight=true;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (moveRight)
        {
            transform.Translate(2*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(-0.7f, 0.7f);
        }
        else
        {
            transform.Translate(-2*Time.deltaTime*speed,0,0);
            transform.localScale = new Vector2(0.7f, 0.7f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Turn"))
        {
            if (moveRight)
            {
                moveRight = false;
            }
            else
            {
                moveRight = true;
            }
        }
    }
}
